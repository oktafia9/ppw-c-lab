from django.shortcuts import render
from django.shortcuts import HttpResponse


def Home(request) :
    return render(request, "home.html")

def AboutMe(request) :
    return render(request, "beranda.html")

def GuestBook(request) :
    return render(request, "buku.html")

def AboutMovie(request) :
    return render(request, "film.html")

def SocialJourney(request) :
    return render(request, "social.html")

# Create your views here.

