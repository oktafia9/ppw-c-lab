from django.urls import path
from . import views

urlpatterns = [
 path('Home/', views.home, name="Home"),
 path('AboutMe/', views.beranda, name="AboutMe"),
 path('GuestBook/', views.buku, name="GuestBook"),
 path('AboutMovie/', views.film, name="AboutMovie"),
 path('SocialJourney/', views.social, name="SocialJourney"),
]
