from django.forms import ModelForm
from .models import Jadwal
from django import forms

class JadwalView(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(JadwalView, self).__init__(*args, **kwargs)

		self.fields['kegiatan'].label = 'Nama Kegiatan'
		self.fields['kegiatan'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan nama kegiatan'}
		self.fields['kegiatan'].error_messages = {'max_length': 'Maksimal karakter adalah 200 karakter.'}

		self.fields['tempat'].label = 'Tempat Kegiatan'
		self.fields['tempat'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan tempat kegiatan'}
		self.fields['tempat'].error_messages = {'max_length': 'Maksimal karakter adalah 200 karakter.'}

		self.fields['kategori'].label = 'Kategori Kegiatan'
		self.fields['kategori'].widget.attrs = {'class': 'form-control', 'placeholder': 'Masukkan kategori kegiatan'}
		self.fields['kategori'].error_messages = {'max_length': 'Maksimal karakter adalah 100 karakter.'}

		self.fields['tanggal'] = forms.DateTimeField( widget=forms.DateTimeInput(attrs={'type': 'datetime-local','class': 'form-control'}),
                                                    label='Waktu Kegiatan', input_formats=['%d/%m/%Y %H:%M'],
                                                    error_messages={'invalid': 'Format tanggal dan waktu tidak valid.'})
	class Meta:
		model = Jadwal
		fields = ['kegiatan', 'tempat', 'kategori', 'tanggal']
