from django import forms
from django.db import models
import datetime
# Create your models here.

class Jadwal(models.Model):
    kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=200)
    tanggal = models.DateTimeField()

    def __str__(self):
        return self.kegiatan

