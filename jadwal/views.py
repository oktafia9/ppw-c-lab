# Create your views here

from django.urls import reverse
from .forms import JadwalView
from .models import Jadwal

from django.http import HttpResponseRedirect
from django.shortcuts import render

from django.contrib import messages
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader

def jadwalinput(request):
	if (request.method == 'POST'):
		form = JadwalView(request.POST or None)
		if form.is_valid():
			form.save()
	else:
		form = JadwalView()
	t = loader.get_template('jadwal.html')
	c = {'form': form,}
	return render(request, 'jadwal.html', c)
	
	
def jadwallist(request):
    form = JadwalView(request.POST or None)
    if(request.method == 'POST'):
        kegiatan = request.POST['kegiatan'] if request.POST['kegiatan'] != "" else "Anonymous"
        tempat = request.POST['tempat'] if request.POST['tempat'] != "" else "Anonymous"
        kategori = request.POST['kategori']
        tanggal = request.POST['tanggal']
        message=Jadwal(kegiatan=kegiatan,tempat=tempat,kategori=kategori,tanggal=tanggal)
        message.save()

        html = Jadwal
        template_name = 'jadwalout.html'
        context_object_name = 'schedule'
        queryset = html.objects.all()
        result = {context_object_name : queryset}
        return render(request, template_name, result)

    else:
        model = Jadwal
        template_name = 'jadwalout.html'
        context_object_name = 'schedule'
        queryset = model.objects.all()

        result = {context_object_name : queryset}
        return render(request, template_name, result)	

def hapusjadwal(request):
    Jadwal.objects.all().delete()
    context ={}
    return render(request, 'jadwalout.html', context)
    

