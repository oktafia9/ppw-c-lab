from django.urls import path
from . import views
from .views import jadwalinput, jadwallist, hapusjadwal

urlpatterns = [
   path('Jadwal/', views.jadwal, name='jadwalinput'),
   path('jadwallist/', views.jadwalout, name='jadwallist'),
   path('hapusjadwal/'views.jadwalout, name='hapusjadwal')
]
