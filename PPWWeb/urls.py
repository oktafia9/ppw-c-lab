"""PPWWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
from ProfileWeb.views import Home as Home
from ProfileWeb.views import AboutMe as AboutMe
from ProfileWeb.views import GuestBook as GuestBook
from ProfileWeb.views import AboutMovie as AboutMovie
from ProfileWeb.views import SocialJourney as SocialJourney
from jadwal.views import jadwalinput as jadwalinput
from jadwal.views import jadwallist as jadwallist
from jadwal.views import hapusjadwal as hapusjadwal


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^home.html', Home, name="Home"),
    re_path(r'^beranda.html', AboutMe, name="AboutMe"),
    re_path(r'^buku.html', GuestBook, name="GuestBook"),
    re_path(r'^film.html', AboutMovie, name="AboutMovie"),
    re_path(r'^social.html', SocialJourney, name="SocialJourney"),
    re_path(r'^jadwal$', jadwalinput, name="jadwalinput"),
	re_path(r'^jadwalout.html$', jadwallist, name="jadwallist"),
    re_path(r'^hapusjadwal', hapusjadwal, name="hapusjadwal"),
]
